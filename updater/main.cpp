#include <QCoreApplication>

#include "util/utils.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Utils::untar_archive();

    Utils::start_app();

    return a.exec();
}

