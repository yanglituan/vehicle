#include <QDebug>
#include <QDir>
#include <QProcess>
#include <QThread>
#include <QCoreApplication>

#include "../vehicle/include/constant.h"
#include "util/utils.h"

Utils::Utils()
{
}

void Utils::untar_archive() {
    qDebug() << "Utils | untar_archive";
    QDir directory(TARGET_DIR);
    if (directory.exists()) {
        directory.rmdir(TARGET_DIR);
    }
    directory.mkdir(TARGET_DIR);

    QThread::sleep(2);

    system(QString("tar -zxvf %1 -C %2").arg(UPDATE_ARCHIVE_FILE,
                                             TARGET_DIR).toLatin1().data());
    system(QString("chmod 777 %1").arg(TARGET_VEHICLE_PATH).toLatin1().data());
}

void Utils::start_app() {
    qDebug() << "Utils | start app";
    system("reboot");
    exit(0);
}

