#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"

#include "mqtt/mqtt_client_update.h"

MqttClientUpdate::MqttClientUpdate(const char *id, const char *topic, const char *host, int port)
    : MqttClient(id, topic, host, port)
{
}

void MqttClientUpdate::on_connect(int rc) {
    LOG(INFO) << "MqttClientUpdate | on_connect | rc" << rc;
    if (rc == 0) {
        this->subscribe_topic();
    }
}

void MqttClientUpdate::on_message(const struct mosquitto_message *message) {
    char *payload = (char*)message->payload;
    int len = message->payloadlen;
    LOG(INFO) << "MqttClientUpdate | on_message | archive";
    MqttObserver::get_instance()->notify_update(payload, len);
}

void MqttClientUpdate::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientUpdate | on_subscribe Success";
}
