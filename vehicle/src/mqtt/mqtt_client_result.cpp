#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"
#include "mqtt/mqtt_client_result.h"

MqttClientResult::MqttClientResult(const char *id, const char *topic, const char *host, int port)
    : MqttClient(id, topic, host, port)
{
}

void MqttClientResult::on_connect(int rc) {
    LOG(INFO) << "MqttClientResult | on_connect | rc" << rc;
}

void MqttClientResult::on_message(const struct mosquitto_message *message) {
    LOG(INFO) << "MqttClientResult | on_message";
}

void MqttClientResult::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientResult | on_subscribe Success";
}
