#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"
#include "mqtt/mqtt_client_picture.h"

MqttClientPicture::MqttClientPicture(const char *id, const char *topic, const char *host, int port)
    : MqttClient(id, topic, host, port)
{
}

void MqttClientPicture::on_connect(int rc) {
    LOG(INFO) << "MqttClientPicture | on_connect | rc" << rc;
    if (rc == 0) {
        this->subscribe_topic();
    }
}

void MqttClientPicture::on_message(const struct mosquitto_message *message) {
    LOG(INFO) << "MqttClientPicture | on_message";
    char *payload = (char*)message->payload;
}

void MqttClientPicture::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientPicture | on_subscribe Success";
}

