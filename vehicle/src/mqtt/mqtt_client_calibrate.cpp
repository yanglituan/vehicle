﻿#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"
#include "mqtt/mqtt_client_calibrate.h"

MqttClientCalibrate::MqttClientCalibrate(const char *id, const char *topic, const char *host, int port)
    : MqttClient(id, topic, host, port)
{
}

void MqttClientCalibrate::on_connect(int rc) {
    LOG(INFO) << "MqttClientCalibrate | on_connect | rc" << rc;
    if (rc == 0) {
        this->subscribe_topic();
    }
}

void MqttClientCalibrate::on_message(const struct mosquitto_message *message) {
    char *payload = (char*)message->payload;
    LOG(INFO) << "MqttClientCalibrate | on_message | payload" << payload;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(payload);
    if (jsonResponse.isEmpty() == true) {
        LOG(INFO) << "MqttClientCalibrate | on_message | payload is not json object";
        return;
    }
    // 解析在线标定数据
    CalibrateEntity calibrate = JsonParser::parse_calibrate(jsonResponse.object());
    // 观察者回调
    MqttObserver::get_instance()->notify_calibrate(calibrate);
}

void MqttClientCalibrate::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientCalibrate | on_subscribe Success";
}
