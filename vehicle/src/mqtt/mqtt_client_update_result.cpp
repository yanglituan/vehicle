#include <glog/logging.h>
#include <QJsonDocument>

#include "entity/mqtt_entity.h"
#include "manager/json_parser.h"
#include "manager/mqtt_observer.h"
#include "mqtt/mqtt_client_update_result.h"

MqttClientUpdateResult::MqttClientUpdateResult(const char *id, const char *topic,
    const char *host, int port) : MqttClient(id, topic, host, port)
{
}

void MqttClientUpdateResult::on_connect(int rc) {
    LOG(INFO) << "MqttClientUpdateResult | on_connect | rc" << rc;
}

void MqttClientUpdateResult::on_message(const struct mosquitto_message *message) {
   LOG(INFO) << "MqttClientUpdateResult | on_message";
}

void MqttClientUpdateResult::on_subscribe(int mid, int qos_count, const int *granted_qos) {
    LOG(INFO) << "MqttClientUpdateResult | on_subscribe Success";
}
