#include <glog/logging.h>
#include "manager/mqtt_observer.h"

MqttObserver* MqttObserver::get_instance() {
    static MqttObserver observer;
    return &observer;
}

MqttObserver::MqttObserver()
{
    this->listener = NULL;
}

void MqttObserver::set_listener(IMqttListener *listener) {
    this->listener = listener;
}


void MqttObserver::notify_config(ConfigEntity config) {
    LOG(INFO) << "MqttObserver | notify_config";
    if (this->listener != NULL) {
        this->listener->on_config(config);
    }
}

void MqttObserver::notify_command(CommandEntity command) {
	 LOG(INFO) << "MqttObserver | notify_command";
    if (this->listener != NULL) {
        this->listener->on_command(command);
    }
}

void MqttObserver::notify_update(char *archive, int len) {
    LOG(INFO) << "MqttObserver | notify_update";
    if (this->listener != NULL) {
        this->listener->on_update(archive, len);
    }
}

void MqttObserver::notify_calibrate(CalibrateEntity calibrate) {
    LOG(INFO) << "MqttObserver | notify_calibrate";
    if (this->listener != NULL) {
        this->listener->on_calibrate(calibrate);
    }
}
