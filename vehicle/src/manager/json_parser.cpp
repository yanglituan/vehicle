﻿#include <glog/logging.h>
#include <QDate>
#include <QTime>
#include <QJsonArray>
#include "manager/json_parser.h"

JsonParser::JsonParser()
{

}

ConfigEntity JsonParser::parse_config(QJsonObject object) {
    ConfigEntity config;
    config.time_cycle = 0;
    config.status_interval = 0;
    config.picture_format = "";
    config.video_resolution = "";

    QJsonValue time_cycle = object.value("time_cycle");
    QJsonValue status_interval = object.value("status_interval");
    QJsonValue picture_format = object.value("picture_format");
    QJsonValue video_resolution = object.value("video_resolution");

    if (time_cycle.isUndefined() == false) {
        config.time_cycle = time_cycle.toInt();
    }

    if (status_interval.isUndefined() == false) {
        config.status_interval = status_interval.toInt();
    }

    if (picture_format.isUndefined() == false) {
        config.picture_format = picture_format.toString();
    }

    if (video_resolution.isUndefined() == false) {
        config.video_resolution = video_resolution.toString();
    }

    return config;
}


CommandEntity JsonParser::parse_command(QJsonObject object) { 
    CommandEntity command;
    command.type = "";
    command.length = 0;

    QJsonValue type = object.value("type");
    QJsonValue length = object.value("length");

    if (type.isUndefined() == false) {
        command.type = type.toString();
    } else {
        LOG(INFO) << "JsonParser | parse_command | type is isUndefined";
    }

    if (length.isUndefined() == false) {
        command.length = length.toInt();
    } else {
        LOG(INFO) << "JsonParser | parse_command | length is isUndefined";
    }

    return command;
}

CalibrateEntity JsonParser::parse_calibrate(QJsonObject object) {
    CalibrateEntity calibration;
    // 解析原图标定点坐标
    QJsonArray target_json = object.value("current").toArray();
    for (int i = 0; i < target_json.size() && i < CALIBRATE_POINT_COUNT; ++i) {
        QJsonObject point_json = target_json.at(i).toObject();
        int index = point_json.value("sequence").toInt(0);
        calibration.target_points[index].setX(point_json.value("longitudinalPosition").toInt(0));
        calibration.target_points[index].setY(point_json.value("lateralPosition").toInt(0));
    }
    // 解析透视图标定点坐标
    QJsonArray prespective_json = object.value("target").toArray();
     for (int i = 0; i < prespective_json.size() && i < CALIBRATE_POINT_COUNT; ++i) {
        QJsonObject point_json = prespective_json.at(i).toObject();
        int index = point_json.value("sequence").toInt(0);
        calibration.prespective_points[index].setX(point_json.value("longitudinalPosition").toInt(0));
        calibration.prespective_points[index].setY(point_json.value("lateralPosition").toInt(0));
    }
    return calibration;
}


QString JsonParser::json_status(StatusEntity status) {
    QJsonObject json;
    json.insert("time_cycle", status.time_cycle);
    json.insert("status", status.status);
    json.insert("mode", status.mode);
    json.insert("version", status.version);

    QJsonDocument document;
    document.setObject(json);
    QByteArray byte_array = document.toJson(QJsonDocument::Compact);
    QString json_str(byte_array);
    LOG(INFO) << "JsonParser | json_status | " << json_str.toStdString();
    return json_str;
}

QJsonObject JsonParser::json_vehicle(VehicleType vehicleType) {
    QJsonObject json;
    json.insert("passenger_cars", vehicleType.passenger_cars);
    json.insert("trucks", vehicleType.trucks);
    json.insert("other", vehicleType.other);
    return json;
}

QString JsonParser::json_result(ResultEntity result) {
    QJsonObject json;
    json.insert("vehicle_num", result.vehicle_num);
    json.insert("time_cycle", result.time_cycle);
    json.insert("vehicle_types", json_vehicle(result.vehicle_types));
    json.insert("traffic_condition", result.traffic_condition);
    json.insert("vehicle_speed", result.vehicle_speed);
    json.insert("weather", result.weather);
    json.insert("current_time", result.current_time);

    QJsonDocument document;
    document.setObject(json);
    QByteArray byte_array = document.toJson(QJsonDocument::Compact);
    QString json_str(byte_array);
    LOG(INFO) << "JsonParser | json_result | " << json_str.toStdString();
    return json_str;
}

QString JsonParser::json_pedestrian(std::vector<AlgorithmResultEntity> *v_result) {
    QJsonObject json; 
    
    QString str_time = QDateTime::currentDateTime().toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    json.insert("time", str_time);

    json.insert("pedestriansNumber", int(v_result->size()));
    QJsonArray json_array;
    for (std::vector<AlgorithmResultEntity>::iterator it = v_result->begin();
        it != v_result->end(); ++it) {
        QJsonObject json_detect, json_position;
        json_detect.insert("id", it->result_id);
        json_detect.insert("estimatedMass", it->mass);
        json_detect.insert("velocity", it->speed);
        json_detect.insert("orientation", it->orientation);
        json_position.insert("longitudinalPosition", it->cali_x);
        json_position.insert("lateralPosition", it->cali_y);
        json_detect.insert("position", json_position);
        json_array.append(json_detect);
    }
    json.insert("pedestrians", json_array);

    QJsonDocument document;
    document.setObject(json);
    QByteArray byte_array = document.toJson(QJsonDocument::Compact);
    QString json_str(byte_array);
    LOG(INFO) << "JsonParser | json_pedestrian | " << json_str.toStdString();
    return json_str;    
}
