﻿#include <glog/logging.h>
#include <QSettings>
#include <QVariant>

#include "constant.h"
#include "manager/config_manager.h"

ConfigManager::ConfigManager()
{
}

ConfigManager *ConfigManager::getInstance() {
    static ConfigManager instance;
    return &instance;
}

AppConfig *ConfigManager::get_app_config() {
    return &app_config;
}

ConfigEntity *ConfigManager::get_ecb_config() {
    return &ecb_config;
}

void ConfigManager::save() {
   QSettings settings(CONFIG_FILE_NAME, QSettings::IniFormat); // 当前目录的INI文件

   settings.beginGroup("app");
   settings.setValue("broker", app_config.broker);
   settings.setValue("port", app_config.port);
   settings.endGroup();

   settings.beginGroup("ecb");
   settings.setValue("time_cycle", (int)ecb_config.time_cycle);
   settings.setValue("status_interval", (int)ecb_config.status_interval);
   settings.setValue("picture_format", ecb_config.picture_format);
   settings.setValue("video_resolution", ecb_config.video_resolution);
   settings.setValue("target_points", ecb_config.target_points);
   settings.setValue("prespective_points", ecb_config.prespective_points);
   settings.endGroup();
}

void ConfigManager::load() {
    QSettings settings(CONFIG_FILE_NAME, QSettings::IniFormat);

    app_config.broker = settings.value("app/broker").toString();
    app_config.port = settings.value("app/port").toInt();
    app_config.confidence = settings.value("app/confidence").toDouble();
    app_config.start_delay = settings.value("app/start_delay").toInt();
    app_config.algorithm_type = settings.value("app/algorithm_type").toInt();
    app_config.show_video = settings.value("app/show_video").toBool();
    app_config.show_detect = settings.value("app/show_detect").toBool();
    app_config.live_camera = settings.value("app/live_camera").toBool();
    app_config.rtsp_camera = settings.value("app/rtsp_camera").toString();

    ecb_config.time_cycle = settings.value("ecb/time_cycle").toInt();
    ecb_config.status_interval = settings.value("ecb/status_interval").toInt();
    ecb_config.picture_format = settings.value("ecb/picture_format").toString();
    ecb_config.video_resolution = settings.value("ecb/video_resolution").toString();
    ecb_config.target_points = settings.value("ecb/target_points").toString();
    ecb_config.prespective_points = settings.value("ecb/prespective_points").toString();

    LOG(INFO) << "ConfigManager | load | broker" << app_config.broker.toStdString();
    LOG(INFO) << "ConfigManager | load | port" << app_config.port;
    LOG(INFO) << "ConfigManager | load | confidence " << app_config.confidence;
    LOG(INFO) << "ConfigManager | load | start_delay" << app_config.start_delay;
    LOG(INFO) << "ConfigManager | load | show_video" << app_config.show_video;
    LOG(INFO) << "ConfigManager | load | show_detect" << app_config.show_detect;
    LOG(INFO) << "ConfigManager | load | live_camera" << app_config.live_camera;
    LOG(INFO) << "ConfigManager | load | rtsp_camera" << app_config.rtsp_camera.toStdString();

    LOG(INFO) << "ConfigManager | load | time_cycle" << ecb_config.time_cycle;
    LOG(INFO) << "ConfigManager | load | status_interval" << ecb_config.status_interval;
    LOG(INFO) << "ConfigManager | load | picture_format" << ecb_config.picture_format.toStdString();
    LOG(INFO) << "ConfigManager | load | video_resolution" << ecb_config.video_resolution.toStdString();
    LOG(INFO) << "ConfigManager | load | target_points" << ecb_config.target_points.toStdString();
    LOG(INFO) << "ConfigManager | load | prespective_points" << ecb_config.prespective_points.toStdString();
}
