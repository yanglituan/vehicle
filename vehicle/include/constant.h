﻿#ifndef CONSTANT_H
#define CONSTANT_H

#define CONFIG_FILE_NAME "config.ini"

#define VERSION_NAME "0.2.5"

#define ALGORITHM_TYPE_VECHICLE_PEDESTRIAN 0
#define ALGORITHM_TYPE_PEDESTRIAN 1

#define UPDATE_CHECKSUM_LEN 16
#define MAX_HOSTNAME_SIZE 256
#define MAX_CLIENT_ID_SIZE 512

#define CONFIG_TOPIC_INDEX 0
#define COMMAND_TOPIC_INDEX 1
#define STATUS_TOPIC_INDEX 2
#define RESULT_TOPIC_INDEX 3
#define PICTURE_TOPIC_INDEX 4
#define VIDEO_TOPIC_INDEX 5
#define UPDATE_TOPIC_INDEX 6
#define UPDATE_RESULT_TOPIC_INDEX 7
#define HUMAN_TOPIC_INDEX 8
#define CAMERA_CALIBRATION 9

#define TOPIC_SIZE 10
#define TOPIC_CONFIG "jetson/config"
#define TOPIC_COMMAND "jetson/command"
#define TOPIC_STATUS "jetson/status"
#define TOPIC_RESULT "jetson/results"
#define TOPIC_PICTURE "jetson/picture"
#define TOPIC_VIDEO "jetson/video"
#define TOPIC_UPDATE "jetson/update"
#define TOPIC_UPDATE_RESULT "jetson/update_result"
#define TOPIC_HUMAN "jetson/results/humanDetection"
#define TOPIC_CALIBRATION "ecb/camera/calibration"

#define UPDATE_ARCHIVE_FILE "/var/tmp/archive.tar.gz"
#define UPDATE_ARCHIVE_DIR  "/var/tmp/archive"
#define UPDATER_PATH "/var/tmp/archive/updater"

#define TARGET_DIR "/home/nvidia/siemens/"
#define TARGET_VEHICLE_PATH "/home/nvidia/siemens/vehicle"

#define MODULE_FILE "Models/300x300_ft/deploy.prototxt"
#define WEIGHT_FILE "Models/300x300_ft/300x300_ft_iter_160000.caffemodel"
#define MEAN_FILE ""
#define MEAN_VALUE "104,117,123"

#define VIDEO_FILE "video.avi"
#define VIDEO_SOURCE_FILE "video_source.avi"

// 行人检测参数定义
#define SSD_RESULT_TIMEOUT_MS 2000
#define ALGORITHM_DETECT_DURATION_MS 500

// 车辆检测参数定义
#define ALGORITHM_VEHICLE_DETECT_DURATION_MS 1000

#define MAX_RESULT_ID 10000
#define MAX_RESULT_BUFFER_SIZE 10
#define TEMPLATE_THRESHOLD 0.4

// 标定的点数量
#define CALIBRATE_POINT_COUNT 4

#endif // CONSTANT_H

