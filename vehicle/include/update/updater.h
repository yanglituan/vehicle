#ifndef UPDATER_H
#define UPDATER_H

#include <QThread>

class Updater : public QThread
{
public:
    Updater(char *archive, int len);

    void untar_archive_file();

    void invoke_external_updater();

    void update();

private:
    char *m_archive;
    int m_len;
};

#endif // UPDATER_H
