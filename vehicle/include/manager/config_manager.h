#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H

#include "entity/mqtt_entity.h"

class ConfigManager
{    
public:
    static ConfigManager *getInstance();

    AppConfig* get_app_config();

    ConfigEntity* get_ecb_config();

    void save();

    void load();

private:
    ConfigManager();

    AppConfig app_config;

    ConfigEntity ecb_config;
};

#endif // CONFIGMANAGER_H
