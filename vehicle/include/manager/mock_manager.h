#ifndef MOCKMANAGER_H
#define MOCKMANAGER_H

#include "manager/mqtt_observer.h"
#include "update/updater.h"

class MockManager : public IMqttListener
{
public:
    static MockManager* get_instance();

    void test_status_interface();

    void test_result_interface();

    void on_config(ConfigEntity config);

    void on_command(CommandEntity command);

    void on_update(char *archive, int len);

    void on_calibrate(CalibrateEntity calibrate);

private:
    MockManager();

    ~MockManager();

    Updater *updater;
};

#endif // MOCKMANAGER_H
