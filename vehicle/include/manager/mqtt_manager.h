#ifndef MQTTMANAGER_H
#define MQTTMANAGER_H

#include <mosquittopp.h>
#include <vector>

#include "constant.h"
#include "entity/algorithm_entity.h"
#include "mqtt/mqtt_client_thread.h"

class StatusEntity;
class ResultEntity;

class MqttManager
{

public:
    static MqttManager *getInstance();

    bool init();

    bool send_status(StatusEntity *status);

    bool send_pedestrian(std::vector<AlgorithmResultEntity> *v_pedestrian);

    bool send_result(ResultEntity *result);

    bool send_picture(void *data, int len);

    bool send_video(void *data, long len);

    bool send_video_file();

    bool send_update_result(QString result);

private:
    MqttManager();

    ~MqttManager();

private:
    char clientId[MAX_CLIENT_ID_SIZE];

    MqttClient *mqtt_clients[TOPIC_SIZE];

    MqttClientThread *mqtt_threads[TOPIC_SIZE];
};

#endif // MQTTMANAGER_H
