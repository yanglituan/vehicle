#ifndef TIMERMANAGER_H
#define TIMERMANAGER_H

#include <QObject>
#include <QTimer>\

class TimerManager : public QObject
{
    Q_OBJECT
public:
    TimerManager();

    static TimerManager* get_instance();

    void start_all_timer();

    void reset_all_timer();

private:
    void start_result_timer();

    void start_status_timer();

signals:
    void reset_timer_sig();

public slots:
    void on_result_timer();

    void on_status_timer();

    void reset_timer_slot();

private:
    QTimer *result_timer;

    QTimer *status_timer;
};

#endif // TIMERMANAGER_H
