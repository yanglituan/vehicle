#ifndef DATAKEEPER_H
#define DATAKEEPER_H

#include "entity/mqtt_entity.h"
#include "entity/algorithm_entity.h"

class DataKeeper
{
public:
    DataKeeper();

    static DataKeeper *get_instance();

    FrameResultEntity* get_last_frame_result();

    void set_last_frame_result(FrameResultEntity frame_result);

    bool check_last_frame();

    void clear_last_frame();

    void clear_result();

    ResultEntity* get_result();

private:
    FrameResultEntity frame_result;

    ResultEntity result;

    bool has_last_frame;
};

#endif // DATAKEEPER_H
