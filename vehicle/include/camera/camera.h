﻿#ifndef CAMERA_H 
#define CAMERA_H

#include <QThread>
#include <QTimer>
#include <QByteArray>
#include <QSharedPointer>
#include <QMetaType>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

class Camera : public QThread
{
    Q_OBJECT

public:
    Camera();
    
    ~Camera();

    bool open(const char *rtsp_addr);

    bool reopen();

    bool is_cam_open();

    bool start_grab();

    void stop_grab();

signals:
    void image_ready(QSharedPointer<Mat> sp_frame);

protected:
    void run();

    void clear();

private:
    bool is_open;

    bool is_run;

    VideoCapture capture;

    QByteArray m_address;

    int invalid_frame_count;
};

#endif 
