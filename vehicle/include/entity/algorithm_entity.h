﻿#ifndef ALGORITHM_ENTITY_H
#define ALGORITHM_ENTITY_H

#include <vector>
#include <QSharedPointer>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "kcf/kcftracker.hpp"

struct AlgorithmResultEntity {
    int image_id;
    int label;
    float score;

    int xmin;
    int ymin;
    int xmax;
    int ymax;

    int width;
    int height;
    int xcenter;
    int ycenter;
    int xfooter;
    int yfooter;

    int area;

    int color;
    int orientation;
    float speed;
    float mass;

    int cali_x;
    int cali_y;

    int result_id;
    bool is_match;

    cv::Rect kcf_roi;
    cv::Rect kcf_ssd_roi;
    QSharedPointer<KCFTracker> sp_tracker;

    qint64 timestamp;
};

struct ObjectEntity {
    int x;
    int y;
    int width;
    int height;
    int id;
    int found;
};

struct FrameResultEntity {
    QSharedPointer<cv::Mat> sp_frame;
};


#endif // ALGORITHM_ENTITY_H

