﻿#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <QSharedPointer>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "constant.h"

using namespace cv;
using namespace std;

class Utils
{
public:
    Utils();

    static QString get_client_id();

    static char *str_to_char_array(QString str);

    static std::vector<uchar> mat_to_image(QSharedPointer<Mat> sp_frame, QString type);

    static std::vector<cv::Point2f> parse_points(QString str_points);

    static QString get_points_str(std::vector<cv::Point2f> points);

    static cv::Point2f parse_point(QString str_point);

    static bool is_detect_pedestrian();
};

#endif // UTILS_H
