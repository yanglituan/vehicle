#ifndef MQTTCLIENTUPDATE_H
#define MQTTCLIENTUPDATE_H

#include "mqtt/mqtt_client_thread.h"

class MqttClientUpdate : public MqttClient
{
public:
    MqttClientUpdate(const char *id, const char *topic, const char *host, int port);

    virtual void on_connect(int rc);

    virtual void on_message(const struct mosquitto_message *message);

    virtual void on_subscribe(int mid, int qos_count, const int *granted_qos);
};

#endif // MQTTCLIENTUPDATE_H
