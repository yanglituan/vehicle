#ifndef MQTTCLIENTSTATUS_H
#define MQTTCLIENTSTATUS_H

#include "mqtt/mqtt_client_thread.h"

class MqttClientStatus : public MqttClient
{
public:
    MqttClientStatus(const char *id, const char *topic, const char *host, int port);

    virtual void on_connect(int rc);

    virtual void on_message(const struct mosquitto_message *message);

    virtual void on_subscribe(int mid, int qos_count, const int *granted_qos);
};

#endif // MQTTCLIENTSTATUS_H
