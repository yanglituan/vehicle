#ifndef MQTTCONFIGCLIENT_H
#define MQTTCONFIGCLIENT_H

#include "mqtt_client_thread.h"

class MqttClientConfig : public MqttClient
{
public:
    MqttClientConfig(const char *id, const char *topic, const char *host, int port);

    virtual void on_connect(int rc);

    virtual void on_message(const struct mosquitto_message *message);

    virtual void on_subscribe(int mid, int qos_count, const int *granted_qos);
};

#endif // MQTTCONFIGCLIENT_H
