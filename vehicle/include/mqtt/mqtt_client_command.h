#ifndef MQTTCLIENT_COMMAND_H 
#define MQTTCLIENT_COMMAND_H

#include "mqtt/mqtt_client_thread.h"

class MqttClientCommand : public MqttClient
{
public:
    MqttClientCommand(const char *id, const char *topic, const char *host, int port);

    virtual void on_connect(int rc);

    virtual void on_message(const struct mosquitto_message *message);

    virtual void on_subscribe(int mid, int qos_count, const int *granted_qos);
	
};

#endif