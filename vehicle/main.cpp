﻿#include <glog/logging.h>
#include <QCoreApplication>
#include "util/run_guard.h"
#include "util/common_utils.h"


int main(int argc, char *argv[])
{
    RunGuard guard("vehicle");
    if (!guard.tryToRun()) {
        return 0;
    }

    FLAGS_logbufsecs = 0;
    FLAGS_max_log_size = 100;
    FLAGS_stop_logging_if_full_disk = true;

    google::InitGoogleLogging(argv[0]);
    google::InstallFailureSignalHandler();

    google::SetStderrLogging(google::GLOG_INFO);
    // google::SetLogDestination(google::GLOG_FATAL, "/home/nvidia/log/vehicle_fatal_");
    // google::SetLogDestination(google::GLOG_ERROR, "/home/nvidia/log/vehicle_error_");
    // google::SetLogDestination(google::GLOG_WARNING, "/home/nvidia/log/vehicle_warn_");
    // google::SetLogDestination(google::GLOG_INFO, "/home/nvidia/log/vehicle_info_");

    QCoreApplication a(argc, argv);
    CommonUtils::init_all_instance();
    return a.exec();
}

